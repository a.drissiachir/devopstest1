import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Utilisé pour définir un contrôleur RESTful
@RequestMapping("/hello") // Mapping de base pour toutes les requête 
public class Endpoint {
    int x,y,z;

    @GetMapping("/coucou") 
    public String sayHello() {
        return "Hello World!"; // Retourne une chaîne de caractères
    }
}